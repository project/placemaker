<?php

/**
 * @file
 * Helper functions for the placemaker module.
 */


/**
 * Send and parse a simple placemaker request.
 */
function _placemaker_request_simple($content, $options) {

  $options['autodisambiguate'] = TRUE;
  $options['output'] = 'xml';
  $results = array();

  if ($response = _placemaker_make_request($content, $options)) {
    $xml = simplexml_load_string($response);
    $results = array();

    foreach ($xml->document->placeDetails as $place_detail) {
      $detail = array_reverse(preg_split("/, /", (string)$place_detail->place->name));
      $results[] = array(  'id' => (string)$place_detail->place->woeId,
              'street' => '', #not yet provided by placemaker
              'additional' => '', #not yet provided by placemaker
              'name' => (string)$place_detail->place->name,
              'type' => (string)$place_detail->place->type,
              'country' => $detail[0],
              'province' => $detail[1],
              'city' => $detail[2],
              'postal_code' => '', #not yet provided by placemaker
              'lat' => (string)$place_detail->place->centroid->latitude,
              'lon' => (string)$place_detail->place->centroid->longitude,
              'confidence' => (string)$place_detail->confidence,
      );
    }
  }
  return $results;
}

/**
 * Send placemaker request
 */
function _placemaker_request_full($content, $options) {
  $response = _placemaker_make_request($content, $options);
  return $reponse;
}

/**
 * Make a placemaker request.
 */
function _placemaker_make_request($content, $options) {
  $placemaker_settings = variable_get('placemaker', '');
  $endpoint = 'http://wherein.yahooapis.com/v1/document';

  $post = 'appid=' . $placemaker_settings['appid'];

  if ($content['content']) {
    $post .= '&documentContent=' . $content['content'];
  }
  if ($content['title']) {
    $post .= '&documentTitle=' . $content['title'];
  }
  if ($content['url']) {
    $post .= '&documentURL=' . $content['url'];
  }
  if ($options['output']) {
    $post .= '&outputType=' . $options['output'];
  }
  if ($options['autodisambiguate'] == FALSE) {
    $post .= '&autoDisambiguate=false';
  }
  if ($options['woeid']) {
    $post .= '&focusWoeid=' . $options['woeid'];
  }
  if ($options['language']) {
    $post .= '&inputLanguage=' . $options['woeid'];
  }
  $post .= '&documentType=' . $content['type'];

  $ch = curl_init($endpoint);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
  curl_setopt($ch, CURLOPT_HEADER, TRUE);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $results = curl_exec($ch);
  curl_close($ch);

// Error handling
// Use regular expressions to extract the code from the header
// Yahoo sends 2 response codes: 100 Continue, then the actual code
  preg_match_all('/HTTP\/....\d\d\d/', $results, $status_code);
  $status_code = array_pop($status_code);
  $code = preg_split("/ /" , $status_code[0]);
  if ($code[1] == 100) {
    $code = preg_split("/ /", $status_code[1]);
  }

 // Check the HTTP Response code and display message if status code is not 200 (OK)
  switch ( $code[1] ) {
    case 200:
    // Success
    break;
    case 503:
      $errmsg = 'Yahoo Service unavailable.';
    break;
    case 403:
      $errmsg = 'Forbidden. You do not have permission to access this resource, or are over your rate limit.';
    break;
    case 400:
      $errmsg = 'Bad request. The parameters passed to the service did not match as expected.';
      break;
    default:
      $errmsg = 'Your call to Yahoo Web Services returned an unexpected HTTP status';
  }
  if ($errmsg) {
    drupal_set_message("Error " . $code[1] . ": " . $errmsg, 'error');
    return FALSE;
  }
  return strstr($results, '<?xml');
}

